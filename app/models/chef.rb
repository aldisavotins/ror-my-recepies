class Chef < ApplicationRecord
  before_save { self.email = email.downcase }
  validates :name, presence: true, length: { maximum: 15 }
  validates :email, presence: true, format: { with: URI::MailTo::EMAIL_REGEXP },
            uniqueness: { case_sensitive: false }

  has_many :recipes, dependent: :destroy
  has_many :comments, dependent: :destroy
  
  has_secure_password
  validates :password, presence: true, length: { minimum: 5 }, allow_nil: true
  
  default_scope -> {order(updated_at: :desc)}
end
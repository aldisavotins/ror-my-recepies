class IngredientsController < ApplicationController
  before_action :set_ingredient, only: [:show, :edit, :update]
  before_action :require_admin, only: [:show, :edit, :update]

  def show
    @ingredient_recipes = @ingredient.recipes.paginate(page: params[:page], per_page: 5)
  end

  def index
    @ingredients = Ingredient.paginate(page: params[:page], per_page: 5)
  end

  def new
    @ingredient = Ingredient.new
  end

  def create
    @ingredient = Ingredient.new(get_ingredient_params)
    if @ingredient.save
      flash[:success] = "Ingredient created successfully"
      redirect_to ingredient_path(@ingredient)
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if @ingredient.update(get_ingredient_params)
      flash[:notice] = "Ingredient updated successfully"
      redirect_to @ingredient
    else
      render 'edit'
    end
  end

  private

  def set_ingredient
    @ingredient = Ingredient.find(params[:id])
  end

  def get_ingredient_params
    params.require(:ingredient).permit(:name)
  end
end
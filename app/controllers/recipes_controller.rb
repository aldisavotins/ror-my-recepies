class RecipesController < ApplicationController
  before_action :initialize_recipe, only:[:show, :update, :edit, :destroy]
  before_action :require_user, except:[:index, :show]
  before_action :require_same_user, only:[:update, :edit, :destroy]

  def index
    @recipes = Recipe.paginate(page: params[:page], per_page: 5)
  end

  def show
    @comments = @recipe.comments.paginate(page: params[:page], per_page: 5)
    @comment = Comment.new
  end

  def new
    @recipe = Recipe.new
  end

  def create
    @recipe = Recipe.new(get_recipe_params)
    @recipe.chef = current_chef
    if @recipe.save
      flash[:success] = "Recipe saved succesfully"
      redirect_to recipe_path(@recipe)
    else
      render 'recipes/new'
    end
  end

  def edit
  end

  def update
    if @recipe.update(get_recipe_params)
      flash[:success] = "Recipe updated succesfully"
      redirect_to recipe_path(@recipe)
    else
      render 'recipes/edit'
    end
  end

  def destroy
    @recipe.destroy
    flash[:success] = "Recipe deleted succesfully"
    redirect_to recipes_path
  end

  private

  def get_recipe_params
    params.require(:recipe).permit(:name, :description, ingredient_ids:[] )
  end

  def initialize_recipe
    @recipe = Recipe.find(params[:id])
  end

  def require_same_user
    unless @recipe.chef == current_chef && !current_chef.admin?
      flash[:danger] = "You can only edit your recipes"
      redirect_to recipes_path
    end
  end
end
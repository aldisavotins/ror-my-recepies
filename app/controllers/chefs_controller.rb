class ChefsController < ApplicationController
  before_action :initialize_chef, only: [:show, :edit, :update, :destroy]
  before_action :require_same_user, only:[:update, :edit]
  before_action :require_admin, only:[:destroy]

  def show
    @chef_recipes = @chef.recipes.paginate(page: params[:page], per_page: 5)
  end

  def new
    @chef = Chef.new
  end

  def index
    @chefs = Chef.paginate(page: params[:page], per_page: 5)
  end

  def edit
  end

  def update
    if @chef.update(get_chef_params)
      flash[:success] = "Your account was updated successfully"
      redirect_to @chef
    else
      render 'chefs/edit'
    end
  end

  def create
    @chef = Chef.new(get_chef_params)
    if @chef.save
      session[:chef_id] = @chef.id
      flash[:success] = "Welcome, #{@chef.name} to MyRecipes app"
      redirect_to chef_path(@chef)
    else
      render 'chefs/new'
    end
  end

  def destroy
    unless @chef.admin?
      @chef.destroy
      flash[:danger] = "Chef was deleted with all asociated recipes"
      redirect_to chefs_path
    end
  end

  private

  def get_chef_params
    params.require(:chef).permit(:name, :email, :password, :password_confirmation)
  end

  def initialize_chef
    @chef = Chef.find(params[:id])
  end

  def require_same_user
    unless @chef == current_chef && !current_chef.admin?
      flash[:danger] = "You can only edit your profile"
      redirect_to chefs_path
    end
  end
end
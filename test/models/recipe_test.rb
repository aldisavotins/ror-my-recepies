require 'test_helper'

class RecipeTest < ActiveSupport::TestCase
  
  def setup
    @chef = Chef.create(name: "test chef", email: "test.chef@test.com", password: "passw", password_confirmation: "passw")    
    @recipe = @chef.recipes.build(name: "Test recipe", description: "Test description")
  end

  test "recipe should be valid" do
    assert @recipe.valid?
  end

  test "name should be present" do
    @recipe.name = ""
    assert @recipe.invalid?
  end

  test "description should be present" do
    @recipe.name = "Test recipe"
    @recipe.description = ""

    assert @recipe.invalid?
  end

  test "description should be longer that 4 chars" do
    @recipe.description = "1234"
    assert @recipe.invalid?

    @recipe.description = "12345"
    assert @recipe.valid?
  end

  test "description should not be longer than 500 chars"  do
    @recipe.description = "1" * 501
    assert @recipe.invalid?
  end
end
require 'test_helper'

class ChefTest < ActiveSupport::TestCase

  def setup
    @chef = Chef.new(name: "test chef", email: "test.chef@test.com", password: "passw", password_confirmation: "passw")
  end

  test "should be valid" do
    assert @chef.valid?
  end

  test "name should be present" do
    @chef.name = ""
    assert @chef.invalid?
    @chef.name = "test chief"
  end

  test "name should not be longer than 15 chars" do
    @chef.name = "1" * 15
    assert @chef.valid?

    @chef.name = "1" * 16
    assert @chef.invalid?
  end

  test "email should be present and valid" do
    @chef.email = ""
    assert @chef.invalid?

    @chef.email = "1234.com"
    assert @chef.invalid?
  end

  test "email should be unique" do
    @chef.email = "test.chef@test.com"
    @chef.save

    @another_chef = Chef.new(name: "another_chef", email: "test.chef@test.com")
    assert @another_chef.invalid?
  end

  test "email should be downcase" do
    mixed_case_email = "TeST.Chef@test.com"
    @chef.email = mixed_case_email
    @chef.save
    assert_equal @chef.email, mixed_case_email.downcase
  end

  test "password should be present" do
    @chef.password = @chef.password_confirmation = " "
    assert @chef.invalid?
  end

  test "password should be at least 5 chars long" do
    @chef.password = @chef.password_confirmation = "a" * 4
    assert @chef.invalid?
    @chef.password = @chef.password_confirmation = "a" * 5
    assert @chef.valid?
  end

  test "should delete associated recipes" do
    @chef.save
    @chef.recipes.create!(name: "testing", description: "testing destroy")
    assert_difference 'Recipe.count', -1 do
      @chef.destroy
    end
  end
end
require 'test_helper'

class RecipesTest < ActionDispatch::IntegrationTest

  def setup
    @chef = Chef.create!(name: "test", email: "test@test.com", password: "passw", password_confirmation: "passw")
    @recipe_one = Recipe.create!(name: "eggs", description: "with milk", chef: @chef)
    @recipe_two = Recipe.create!(name: "potatoes", description: "with beef", chef: @chef)
  end

  test "should get recipes index" do
    get recipes_path
    assert_response :success
  end

  test "should get recipes list" do
    get recipes_path
    assert_template 'recipes/index'
    assert_select "a[href=?]", recipe_path(@recipe_one), text: @recipe_one.name
    assert_select "a[href=?]", recipe_path(@recipe_two), text: @recipe_two.name
  end

  test "should get recipe show" do
    sign_in_as_user(@chef, @chef.password)
    get recipe_path(@recipe_one)
    assert_template 'recipes/show'
    assert_match @recipe_one.name, response.body
    assert_match @recipe_one.description, response.body
    assert_match @chef.name, response.body
    assert_select 'a[href=?]', edit_recipe_path(@recipe_one), text: 'Edit this recipe'
    assert_select 'a[href=?]', destroy_recipe_path(@recipe_one), text: 'Delete this recipe'
  end

  test "create new and valid recipe" do
    sign_in_as_user(@chef, @chef.password)
    get new_recipe_path
    assert_template 'recipes/new'
    recipe_name = "test recipe"
    recipe_description = "recipe description"
    assert_difference 'Recipe.count', 1 do
      post recipes_path, params: { recipe: { name: recipe_name, description: recipe_description } }
    end
    follow_redirect!
    assert_match recipe_name.capitalize, response.body
    assert_match recipe_description, response.body
  end

  test "reject invalid recipe creation" do
    sign_in_as_user(@chef, @chef.password)
    get new_recipe_path
    assert_template 'recipes/new'
    assert_no_difference 'Recipe.count' do
      post recipes_path, params: { recipe: {name: "", description: ""} }
    end
    assert_template 'recipes/new'
    assert_select 'h2.panel-title'
    assert_select 'div.panel-body'
  end
end

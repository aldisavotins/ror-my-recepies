require 'test_helper'

class ChefEditTest < ActionDispatch::IntegrationTest
  
  def setup
    @chef = Chef.create!(name: "test", email: "test@test.com", password: "passw", password_confirmation: "passw")
  end

  test "reject invalid edit" do
    sign_in_as_user(@chef, @chef.password)
    get edit_chef_path(@chef)
    assert_template 'chefs/edit'
    patch chef_path(@chef), params: { chef: { name: ' ', email: 'test@test.com'} }
    assert_template 'chefs/edit'
    assert_select 'h2.panel-title'
    assert_select 'div.panel-body'
  end

  test "accept valid edit" do
    sign_in_as_user(@chef, @chef.password)
    get edit_chef_path(@chef)
    patch chef_path(@chef), params: { chef: { name: 'test1', email: 'test1@test.com',
                                      password: 'password', password_confirmation: 'password' } }
    assert_redirected_to @chef
    assert_not flash.empty?
    @chef.reload
    assert_match @chef.name, 'test1'
    assert_match @chef.email, 'test1@test.com'
  end
end

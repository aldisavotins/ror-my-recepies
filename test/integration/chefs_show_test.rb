require 'test_helper'

class ChefsShowTest < ActionDispatch::IntegrationTest
  
  def setup
    @chef = Chef.create!(name: "test", email: "test@test.com", password: "passw", password_confirmation: "passw")
    @recipe_one = Recipe.create!(name: "eggs", description: "with milk", chef: @chef)
    @recipe_two = Recipe.create!(name: "potatoes", description: "with beef", chef: @chef)
  end

  test "should get chefs show" do
    get chefs_path(@chef)
    assert_template 'chefs/show'
    assert_select "a[href=?]", recipe_path(@recipe_one), text: @recipe_one.name
    assert_select "a[href=?]", recipe_path(@recipe_two), text: @recipe_two.name
    assert_match @recipe_one.description, response.body
    assert_match @recipe_two.description, response.body
    assert_match @chef.name, response.body
  end


  test "should delete chef" do
    get chefs_path
    assert_template 'chefs/index'
    assert_difference "Chef.count", -1 do
      delete chefs_path(@chef)
    end
    assert_redirected_to chefs_path
    assert_not flash.empty?
  end

end

require 'test_helper'

class RecipesEditTest < ActionDispatch::IntegrationTest

  def setup
    @chef = Chef.create!(name: "test", email: "test@test.com", password: "passw", password_confirmation: "passw")
    @recipe = Recipe.create!(name: "eggs", description: "with milk", chef: @chef)
  end

  test "reject invalid recipe update" do
    sign_in_as_user(@chef, @chef.password)
    get edit_recipe_path(@recipe)
    assert_template 'recipes/edit'
    patch recipe_path(@recipe), params: { recipe: { name: "", description: "test description" } }
    assert_template 'recipes/edit'
    assert_select 'h2.panel-title'
    assert_select 'div.panel-body'
  end

  test "recipe edited successfully" do
    sign_in_as_user(@chef, @chef.password)
    get edit_recipe_path(@recipe)
    assert_template 'recipes/edit'
    patch recipe_path(@recipe), params: { recipe: { name: "test name", description: "test description" } }
    assert_redirected_to @recipe
    @recipe.reload
    assert_match @recipe.name.capitalize, response.body
    assert_match @recipe.description, response.body
  end
end

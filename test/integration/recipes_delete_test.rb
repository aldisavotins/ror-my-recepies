require 'test_helper'

class RecipesDeleteTest < ActionDispatch::IntegrationTest

  def setup
    @chef = Chef.create!(name: "test", email: "test@test.com", password: "passw", password_confirmation: "passw")
    @recipe = Recipe.create!(name: "eggs", description: "with milk", chef: @chef)
  end

  test "should delete recipe" do
    sign_in_as_user(@chef, @chef.password)
    get recipe_path(@recipe)
    assert_template 'recipes/show'
    assert_select 'a[href=?]', recipe_path(@recipe), text: "Delete recipe"
    assert_difference 'Recipe.count', -1 do
      delete recipe_path(@recipe)
    end
    assert_redirected_to recipes_path
    assert_not flash.empty?
  end
end

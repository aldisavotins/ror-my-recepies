class CreateComments < ActiveRecord::Migration[6.0]
  def change
    create_table :comments do |t|
      t.integer :chef_id
      t.integer :recipe_id
      t.text :description
      t.timestamps
    end
  end
end

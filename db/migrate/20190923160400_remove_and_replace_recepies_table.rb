class RemoveAndReplaceRecepiesTable < ActiveRecord::Migration[6.0]

  def down
    drop_table :recepies
  end

  def up
    create_table :recipes do |t|
      t.string :name
      t.text :description
      t.timestamps
    end
  end
end
